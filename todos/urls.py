from django.urls import path
from todos.views import show_todo_lists, show_details, create_list, edit_list, delete_list, create_item, edit_task

urlpatterns = [
    path("", show_todo_lists, name="show_lists"),
    path('<int:id>/', show_details, name="show_details"),
    path("create/", create_list, name="create_list"),
    path("<int:id>/edit/", edit_list, name="edit_list"),
    path("<int:id>/delete/", delete_list, name="delete_list"),
    path("<int:id>/add/", create_item, name="add_item"),
    path("items/<int:id>/edit/", edit_task, name="edit_task"),
]
