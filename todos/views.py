from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoItem, TodoList
from todos.forms import ListForm, ItemForm
# Create your views here.


def show_todo_lists(request):
    lists = TodoList.objects.all()
    tasks = TodoItem.objects.all()
    context = {
        "todo_lists": lists,
        "tasks": tasks,
    }
    return render(request, "lists/userlists.html", context)


def show_details(request, id):
    list = get_object_or_404(TodoList, id=id)
    context = {
        "details": list,
    }
    return render(request, "lists/detail.html", context)

def create_list(request):
    if request.method == "POST":
        form = ListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("show_lists")
    else:
        form = ListForm()
    context = {
        "form": form,
    }
    return render(request, "lists/create.html", context)

def edit_list(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = ListForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("show_details", id=id)
    else:
        form = ListForm(instance=list)
    context = {
        "todo_list": list,
        "form": form,
    } 
    return render(request, "lists/edit.html", context)

def edit_task(request, id):
    task = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=task)
        if form.is_valid():
            form.save()
            return redirect("show_details", id=id)
    else:
        form = ItemForm(instance=task)
    context = {
            "task": task,
            "form": form,
            
    }
    return render(request, "lists/edititem.html", context)

def delete_list(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        list.delete()
        return redirect("show_lists")
    context = {
        "list": list,
    }
    return render(request, "lists/delete.html", context)

def create_item(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("show_details", id)
    else:
        form = ItemForm()
    context = {
        "form": form,
        "list": list,
    }
    return render(request, "lists/additem.html", context)